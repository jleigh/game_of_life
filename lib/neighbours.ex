defmodule Neighbours do
  def find(cell_index, grid) do
    index = zero_index_to_one(cell_index)

    List.insert_at([], -1, vertical_neighbours(index, grid))
    |> List.insert_at(-1, horizontal_neighbours(index, grid))
    |> List.insert_at(-1, north_diagonal_neighbours(index, grid))
    |> List.insert_at(-1, south_diagonal_neighbours(index, grid))
    |> List.flatten()
    |> Enum.reject(fn x -> x == nil end)
  end

  defp vertical_neighbours(cell_index, grid) do
    northern_index = cell_index - width(grid)
    southern_index = cell_index + width(grid)
    find_vertical_neighbours(grid, southern_index, northern_index)
  end

  defp horizontal_neighbours(cell_index, grid) do
    eastern_index = cell_index + 1
    western_index = cell_index - 1
    find_neighbours(grid, western_index, eastern_index, cell_index)
  end

  defp north_diagonal_neighbours(cell_index, grid) do
    north_east_index = cell_index - width(grid) + 1
    north_west_index = cell_index - width(grid) - 1
    find_neighbours(grid, north_west_index, north_east_index, cell_index)
  end

  defp south_diagonal_neighbours(cell_index, grid) do
    south_east_index = cell_index + width(grid) + 1
    south_west_index = cell_index + width(grid) - 1
    find_neighbours(grid, south_west_index, south_east_index, cell_index)
  end

  defp find_vertical_neighbours(grid, left_index, right_index) do
    right_neighbour = if within_bounds?(right_index), do: Enum.at(grid, right_index), else: nil
    left_neighbour = if within_bounds?(left_index), do: Enum.at(grid, left_index), else: nil
    [right_neighbour, left_neighbour]
  end

  defp find_neighbours(grid, left_index, right_index, cell_index) do
    right_neighbour =
      if right_neighbour?(right_index, cell_index, grid),
        do: Enum.at(grid, right_index),
        else: nil

    left_neighbour =
      if left_neighbour?(left_index, cell_index, grid), do: Enum.at(grid, left_index), else: nil

    [right_neighbour, left_neighbour]
  end

  defp right_neighbour?(right_index, cell_index, grid) do
    within_bounds?(right_index) && !right_edge?(cell_index, width(grid))
  end

  defp left_neighbour?(left_index, cell_index, grid) do
    within_bounds?(left_index) && !left_edge?(cell_index, width(grid))
  end

  defp width(grid) do
    length(grid) |> :math.sqrt() |> trunc
  end

  defp within_bounds?(index) do
    index >= 0
  end

  defp right_edge?(index, width) do
    rem(index + 1, width) == 0
  end

  defp left_edge?(index, width) do
    rem(index, width) == 0
  end

  defp zero_index_to_one(index) do
    index - 1
  end
end
