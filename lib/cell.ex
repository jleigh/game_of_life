defmodule Cell do
  def live(cell) do
    cell |> Map.replace!(:alive, true)
  end

  def die(cell) do
    cell |> Map.replace!(:alive, false)
  end

  def character(cell) do
    if cell.alive == true, do: "*", else: "."
  end
end
