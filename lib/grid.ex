defmodule Grid do
  @min_width 4

  def build(width, seeds) do
    validate_data(width, seeds)

    cell = %{alive: false, neighbour_count: 0}
    live_cell = Cell.live(cell)
    for {_, index} <- with_index(width) do
      if Enum.member?(seeds, index), do: live_cell, else: cell
    end
  end

  def header(generation) do
    if generation < 1, do: raise(RuntimeError, "generation must be greater than 0")
    "===============generation: #{generation}======================"
  end

  def display(grid, width) do
    grid_string = for {cell, index} <- Enum.with_index(grid, 1), do: to_string(cell, width, index)
    Enum.join(grid_string)
  end

  defp validate_data(width, seeds) do
    if width < @min_width, do: raise(RuntimeError, "width must be greater than or equal to 4")
    if Enum.empty?(seeds), do: raise(RuntimeError, "Seeds must not be empty")
  end

  defp to_string(cell, width, index) do
    character = Cell.character(cell)
    if end_line?(index, width), do: "#{character}\n", else: character
  end

  defp end_line?(index, width) do
    rem(index, width) == 0 && index != size(width)
  end

  defp size(width) do
    width * width
  end

  defp with_index(width) do
    Enum.with_index(1..size(width), 1)
  end
end