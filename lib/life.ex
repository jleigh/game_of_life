defmodule Life do
  @final_generation 6

  def start(generation, grid, width) do
    grid_display = display(generation, grid, width)
    IO.puts(grid_display)

    new_grid = tick(grid)
    Process.sleep(1000)
    new_gen = generation + 1
    if new_gen <= @final_generation, do: start(new_gen, new_grid, width), else: grid_display
  end

  def tick(grid) do
    for {cell, index} <- with_index(grid) do
      neighbours = Neighbours.find(index, grid)
      live_neighbours = Enum.reject(neighbours, fn neighbour -> !neighbour.alive end)
      count = length(live_neighbours)
      apply_rules(cell, count)
    end
  end

  defp apply_rules(cell, count) do
    cond do
      death?(count) ->
        Cell.die(cell)

      birth?(cell, count) ->
        Cell.live(cell)

      true ->
        cell
    end
  end

  defp death?(count) do
    underpopulated?(count) || overpopulated?(count)
  end

  defp birth?(cell, count) do
    !cell.alive && count == 3
  end

  defp underpopulated?(count) do
    count < 2
  end

  defp overpopulated?(count) do
    count > 3
  end

  defp with_index(grid) do
    Enum.with_index(grid, 1)
  end

  defp display(generation, grid, width) do
    header = Grid.header(generation)
    display = Grid.display(grid, width)
    "#{header}\n#{display}"
  end
end
