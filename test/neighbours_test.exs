defmodule NeighboursTest do
  use ExUnit.Case

  describe "find" do
    setup do
      seeds = [1, 3, 5, 8, 10, 11, 12, 13, 14]
      %{grid: Grid.build(4, seeds)}
    end

    test "when cell is on the corner of grid returns a list 3 cells", context do
      neighbours = Neighbours.find(16, context[:grid])

      expected = [
        %{alive: true, neighbour_count: 0},
        %{alive: false, neighbour_count: 0},
        %{alive: true, neighbour_count: 0}
      ]

      assert neighbours == expected
    end

    test "when cell is on the left edge of grid returns a list 5 cells", context do
      neighbours = Neighbours.find(5, context[:grid])

      expected = [
        %{alive: true, neighbour_count: 0},
        %{alive: false, neighbour_count: 0},
        %{alive: false, neighbour_count: 0},
        %{alive: false, neighbour_count: 0},
        %{alive: true, neighbour_count: 0}
      ]

      assert neighbours == expected
    end

    test "when cell is on the right edge of grid returns a list 5 cells", context do
      neighbours = Neighbours.find(8, context[:grid])

      expected = [
        %{alive: false, neighbour_count: 0},
        %{alive: true, neighbour_count: 0},
        %{alive: false, neighbour_count: 0},
        %{alive: true, neighbour_count: 0},
        %{alive: true, neighbour_count: 0}
      ]

      assert neighbours == expected
    end

    test "when cell is on the north edge of grid returns a list 5 cells", context do
      neighbours = Neighbours.find(2, context[:grid])

      expected = [
        %{alive: false, neighbour_count: 0},
        %{alive: true, neighbour_count: 0},
        %{alive: true, neighbour_count: 0},
        %{alive: false, neighbour_count: 0},
        %{alive: true, neighbour_count: 0}
      ]

      assert neighbours == expected
    end

    test "when cell is on the south edge of grid returns a list 5 cells", context do
      neighbours = Neighbours.find(15, context[:grid])

      expected = [
        %{alive: true, neighbour_count: 0},
        %{alive: false, neighbour_count: 0},
        %{alive: true, neighbour_count: 0},
        %{alive: true, neighbour_count: 0},
        %{alive: true, neighbour_count: 0}
      ]

      assert neighbours == expected
    end

    test "when cell is not on the edge or corner of grid it returns a list of 8 cells", context do
      neighbours = Neighbours.find(6, context[:grid])

      expected = [
        %{alive: false, neighbour_count: 0},
        %{alive: true, neighbour_count: 0},
        %{alive: false, neighbour_count: 0},
        %{alive: true, neighbour_count: 0},
        %{alive: true, neighbour_count: 0},
        %{alive: true, neighbour_count: 0},
        %{alive: true, neighbour_count: 0},
        %{alive: false, neighbour_count: 0}
      ]

      assert neighbours == expected
    end
  end
end
