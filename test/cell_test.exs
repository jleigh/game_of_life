defmodule CellTest do
  use ExUnit.Case

  describe "live" do
    test "sets alive key to true" do
      cell = %{alive: false, neighbour_count: 2}
      new_cell = Cell.live(cell)
      assert new_cell.alive == true
      assert new_cell.neighbour_count == 2
    end
  end

  describe "die" do
    test "sets alive key to false" do
      cell = %{alive: true, neighbour_count: 4}
      new_cell = Cell.die(cell)
      assert new_cell.alive == false
      assert new_cell.neighbour_count == 4
    end
  end

  describe "character" do
    test "returns '*' when living" do
      cell = %{alive: true, neighbour_count: 2}
      assert Cell.character(cell) == "*"
    end

    test "returns '.' when not living" do
      cell = %{alive: false, neighbour_count: 4}
      assert Cell.character(cell) == "."
    end
  end
end
