defmodule LifeTest do
  use ExUnit.Case

  setup_all do
    seeds = [1, 3, 5, 8, 10, 11, 12, 13, 14]
    %{grid: Grid.build(4, seeds)}
  end

  describe "start" do
    test "returns the final generation given a width and a list of indexes for living cells", context do
      final_grid = Life.start(1, context[:grid], 4)
      assert final_grid == "===============generation: 6======================\n....\n.**.\n*..*\n.**."
    end
  end

  describe "tick" do
    test "takes previous generation and returns next", context do
      new_grid = Life.tick(context[:grid])
      grid_string = Grid.display(new_grid, 4)
      assert grid_string == ".*..\n*..*\n...*\n**.."
    end
  end
end