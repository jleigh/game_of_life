defmodule GridTest do
  use ExUnit.Case

  describe "build" do
    test "given the width < the min_rows_and_columns it throws an error" do
      assert_raise RuntimeError, "width must be greater than or equal to 4", fn ->
        seeds = [2, 3, 4, 6, 7]
        Grid.build(0, seeds)
      end
    end

    test "given an empty list of seed indexes it throws an error" do
      assert_raise RuntimeError, "Seeds must not be empty", fn ->
        seeds = []
        Grid.build(5, seeds)
      end
    end

    test "given a grid of cells and positions it returns a list with some live cells" do
      seeds = [2, 3, 4, 6, 7]
      grid = Grid.build(4, seeds)

      ele = Enum.at(grid, 0)
      ele1 = Enum.at(grid, 1)
      ele2 = Enum.at(grid, 2)
      ele3 = Enum.at(grid, 3)
      ele4 = Enum.at(grid, 5)
      ele5 = Enum.at(grid, 6)

      assert Map.get(ele, :alive) == false
      assert Map.get(ele1, :alive) == true
      assert Map.get(ele2, :alive) == true
      assert Map.get(ele3, :alive) == true
      assert Map.get(ele4, :alive) == true
      assert Map.get(ele5, :alive) == true
    end
  end

  describe "header" do
    test "raises error if generation is < 1" do
      assert_raise RuntimeError, "generation must be greater than 0", fn ->
        Grid.header(0)
      end
    end

    test "takes generation and returns header string" do
      header = Grid.header(2)
      assert header == "===============generation: 2======================"
    end
  end

  describe "display" do
    test "raises error if grid is empty" do
      assert_raise RuntimeError, "generation must be greater than 0", fn ->
        Grid.header(0)
      end
    end

    test "takes list of cells and returns grid visulization as " do
      seeds = [2, 3, 4, 6, 7, 10, 15]
      grid = Grid.build(4, seeds)
      display = Grid.display(grid, 4)
      assert display == ".***\n.**.\n.*..\n..*."
    end
  end
end
